import pandas as pd
import numpy as np
import matplotlib as plt
np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
df = pd.read_excel("./university data.xlsx")
df = df[['name','CS Score (USNews)','Research Overhead %','Admin Base Pay$','Tuition(out-state)$']]

#Stats Calculation Task1-3
cs_score = df['CS Score (USNews)']
mu1 = cs_score.mean()
var1 = cs_score.var()
sigma1 = cs_score.std()

research_overhead = df['Research Overhead %']
mu2 = research_overhead.mean()
var2 = research_overhead.var()
sigma2 = research_overhead.std()

admin_base_pay = df['Admin Base Pay$']
mu3 = admin_base_pay.mean()
var3 = admin_base_pay.var()
sigma3 = admin_base_pay.std()

tuition = df['Tuition(out-state)$']
mu4 = tuition.mean()
var4 = tuition.var()
sigma4 = tuition.std()

covarianceMat = df.cov().as_matrix();
correlationMat = df.corr().as_matrix()

#Log Likelihood Calculation for Independent Case
from scipy.stats import norm
cs_score_as_list = cs_score.tolist()[:cs_score.count()];
cs_score_logpdf = sum(norm.logpdf(cs_score_as_list,mu1,sigma1));

research_overhead_as_list = research_overhead.tolist()[:research_overhead.count()];
research_overhead_logpdf = sum(norm.logpdf(research_overhead_as_list,mu2,sigma2));

admin_base_pay_as_list = admin_base_pay.tolist()[:admin_base_pay.count()];
admin_base_pay_logpdf = sum(norm.logpdf(admin_base_pay_as_list,mu3,sigma3));

tuition_as_list = tuition.tolist()[:tuition.count()];
tuition_logpdf = sum(norm.logpdf(tuition_as_list,mu4,sigma4));

logLikelihood = cs_score_logpdf + research_overhead_logpdf + admin_base_pay_logpdf + tuition_logpdf

#Log Likelihood calculation for dependent multivariate case
def multivariate_pdf(X,mu,cov):
    Xcount = X.shape[0];
    mucount = mu.size;
    covcount = cov.shape[0];
    if(Xcount != mucount != sigcount != covcount):
        return
    cov_inverse = np.linalg.inv(cov)
    constant = ((2*np.pi)**(Xcount/2))*(np.linalg.det(cov))**(0.5)
    pdf=[]
    for i in range(X.shape[1]):
        x = X[:,i];
        mat1 = x - mu;
        p = np.exp(-0.5 * np.matmul(np.matmul(mat1.transpose(),cov_inverse),mat1))/constant #use repmat
        pdf.append(p)
    return pdf

def multivariate_loglikelihood(X,mu,cov):
    pdf = multivariate_pdf(X,mu,covarianceMat)
    loglikelihood=0
    for val in pdf:
        loglikelihood = loglikelihood + np.log(val)
    return loglikelihood
    
X=np.array([cs_score_as_list,research_overhead_as_list,admin_base_pay_as_list,tuition_as_list]);
mu = np.array([mu1,mu2,mu3,mu4]);
sig = np.array([sigma1,sigma2,sigma3,sigma4]);
multivariateLoglikelihood = multivariate_loglikelihood(X,mu,covarianceMat)


#Bayesian Log Likelihood calculations
def pdf_summation_calc(beta,X,y):
    newX = X
    [N,k] = X.shape
    newbeta = beta
    np.insert(newbeta,k,-1)
    newX = np.insert(newX,0, y,axis=1)
    m = np.multiply(newX,newbeta[:,np.newaxis])
    summat = np.sum(m,axis=0);
    return np.sum(np.square(summat))

def baysian_log_likelihood(y,X):
    N = y.size
    x0 = np.ones(N)
    X=np.insert(X,0,x0,axis=1)
    k = X.shape[1]
    A = np.zeros((k,k))
    for i in range(k):
        for j in range(k):
            val = np.matmul(X[i],X[j].transpose())
            A[i,j] = val;
            if(i != j):
                A[j,i] = val;
    Ainv = np.linalg.inv(A)
    Y = np.matmul(y.transpose(),X)
    beta = np.matmul(Ainv,Y.transpose());
    pdf_summation = pdf_summation_calc(beta,X,y)
    var = pdf_summation/N
    return (-(np.log(2*np.pi*var))/2) - (pdf_summation/(2*var))

BNgraph = np.array([[0,1,0,0],[0,0,0,0],[0,0,0,0],[0,0,1,0]]);
#Calculation for the above Bayesian network
X=np.empty([len(research_overhead_as_list),1])
X[:,0] = np.array(research_overhead_as_list)
y=np.empty([len(cs_score_as_list),1])
y[:,0] = np.array(cs_score_as_list)

cs_score_logpdf = baysian_log_likelihood(y,X)

X=np.empty([len(admin_base_pay_as_list),1])
X[:,0] = np.array(admin_base_pay_as_list)
y=np.empty([len(tuition_as_list),1])
y[:,0] = np.array(tuition_as_list)

tuition_logpdf = baysian_log_likelihood(y,X)

BNlogLikelihood = cs_score_logpdf + research_overhead_logpdf + admin_base_pay_logpdf + tuition_logpdf



#printing all the calculated values
print("UBitName = kpanneer")
print("personNumber = 50248889")
print ("mu1 = {:.3f}".format(mu1))
print ("mu2 = {:.3f}".format(mu2))
print ("mu3 = {:.3f}".format(mu3))
print ("mu4 = {:.3f}".format(mu4))

print ("var1 = {:.3f}".format(var1))
print ("var2 = {:.3f}".format(var2))
print ("var3 = {:.3f}".format(var3))
print ("var4 = {:.3f}".format(var4))

print ('sigma1 = {:.3f}'.format(sigma1))
print ('sigma2 = {:.3f}'.format(sigma2))
print ('sigma3 = {:.3f}'.format(sigma3))
print ('sigma4 = {:.3f}'.format(sigma4))

print ('correlationMat = \n', correlationMat)
print ('covarianceMat = \n', covarianceMat)
print ('logLikelihood = {:.3f}'.format(logLikelihood))
print ('multiVariateLogLikelihood = {:.3f}'.format(multivariateLoglikelihood))
print ('BNgraph = \n', BNgraph)
print ('BNlogLikelihood = {:.3f}'.format(BNlogLikelihood))